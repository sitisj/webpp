<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="en" />

		<!-- blueprint CSS framework -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
		<!--[if lt IE 8]>
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
		<![endif]-->

		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

		<link href="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirCSS'];?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirCSS'];?>/css/style.css" rel="stylesheet" type ="text/css">

		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirJS'];?>/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirJS'];?>/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirJS'];?>/js/scripts.js"></script>
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	</head>

	<body>
		<header>
			<div id="main-nav" class="navbar navbar-inverse bs-docs-nav" role="banner">
	    		<div class="container" id="menu">
	        	<div class="navbar-header">
	        		<a href="<?php echo Yii::app()->request->baseUrl;?>" class="navbar-brand">
	            		<img src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirWebImages'];?>/images/logohome.png" height="65" width="400" alt="PP">
	        		</a>
	        	</div>
	        	<nav class="navbar-collapse bs-navbar-collapse collapse" role="navigation" style="height : 1px;"></nav>
		          	<ul class="nav navbar-nav navbar-right responsive-nav main-nav-list">
		            	<li>
		            		<a href="<?php echo Yii::app()->params['webURL'] ;?>">Home</a>
		            	</li>
		            	<li>
		             		<a href="<?php echo Yii::app()->params['webURL'] ;?>cp/index">Company Profile</a>
		            	</li>
		            	<li>
		              		<a href="<?php echo Yii::app()->params['webURL'] ;?>menu/projects">Projects</a>
		            	</li>
		            	<li>
		              		<a href="<?php echo Yii::app()->params['webURL'] ;?>menu/contacts">Contacts</a>
		            	</li>
		          	</ul>
	        	</nav>
	    	</div>
		</header>
	</body>
</html>

