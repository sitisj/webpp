<?php
/* @var $this CpController */

/*$this->breadcrumbs=array(
	'Company Profile',
);*/
$this->pageTitle=Yii::app()->name . ' | Company Profile';
?>
<?php $this->renderPartial('//cp/submenu'); ?>

<div class="container">
	<h1>History of Corporate</h1>
	<span class="contentcalibri5">
		<p style="text-align: justify;">
			<i style="font-weight: bold;">1953</i>
			<br>
			"PT PP (Persero) was established under the name of NV Pembangunan Perumahan based on the Notarial Deed No. 48 of August 26, 1953. At the time it was established PT PP (Persero) was entrusted to build houses for the officers of PT Semen Gresik Tbk, a subsidiary of BAPINDO in Gresik. Along with increased trust, PT PP (Persero) received the duty to construct large projects that were related to war compensations the Government of Japan paid to the Republic of Indonesia, namely : - Hotel Indonesia, Bali Beach Hotel, - Ambarukmo Palace Hotel and - Samudera Beach Hotel."
			<br>
			<i style="font-weight: bold;">1960</i>
			<br>
			"In compliance to Government Regulation No. 63 year 1960, PN (Perusahaan Negara) Pembangunan Perumahan changed into PN Pembangunan Perumahan. "
			<br>
			<i style="font-weight: bold;">1962</i>
			<br>
			"In 1962, PT PP (Persero) completed the construction of Hotel Indonesia, a 14 stories with 427 rooms, which at the time was the highest building in Indonesia."
			<br>
			<i style="font-weight: bold;">1971</i>
			<br>
			"In compliance to the Government Regulation No. 39 year 1971, PN Pembangunan Perumahan changed and become PT Pembangunan Perumahan (Persero), which was legalized through the Deed No. 78 dated March 15, 1973. The Company's core business was construction services. "
			<br>
			<i style="font-weight: bold;">1991-2007</i>
			<br>
			"For more than five decades, PT PP (Persero) has been a key player in the national construction business. Several mega projects have been constructed in that period. Subsequently, starting in 1991, PT PP (Persero) diversified its business, including office space rental at Plaza PP and realty business development in the area of Cibubur, and also the establishment of several subsidiaries through partnerships with foreign companies, among others PT PPTaisei Indonesia Construction and PT Mitracipta Polasarana."
			<br>
			<i style="font-weight: bold;">2009</i>
			<br>
			"In 2009, in line with business growth and increaseingly strong financial condition, then PT PP (Persero) to prepare a transformation where in 2009 PT PP (Persero) will implement the program in Public Offering Shares to the Public (Initial Public Offering/IPO). Where implementation of program IPO of PT PP (Persero) has received approval from the Government of the Republic of Indonesia in accordance with the Indonesian Goverenment Regulation No. 76 Year 2009 in Amendment of State Shareholding Structure through Issuance and Sale of New Share in the Company (Persero) PT Pembangunan Perumahan dated December 28th 2009."
			<br>
			<i style="font-weight: bold;">2010</i>
			<br>
			"With new Regulation from the Government regarding the Changing of the Ownership Structure of State's Shares, therefore on February 9th 2010 PT PP (Persero) has fulfilled the listing requirement in PT Bursa Efek Indonesia (BEI). Commencing from the abovementioned date, PT PP (Persero) Tbk shares have officially been listed and could be traded in Bursa Efek Indonesia (BEI)."
			<br>
			<i style="font-weight: bold;">2011</i>
			<br>
			"The Company succeeded in completing first investment project, that is Gas Power Plant (PLTG) with 65 MegaWatt power at Talang Duku, South Sumatera. The project is inaugurated by Director of Perusahaan Listrik Negara (PLN) in October 2011 and also contributed during the SEA Games event in Palembang. Therefore, the Company exercised business activity diverisification, that is Engineering, Procurement & Contruction (EPC) and Investment."
			<br>
			<i style="font-weight: bold;">2012</i>
			<br>
			"The Company is mandated to perform several infrastructures project in Indonesia namely New Tanjung Priok with contract value amounted to Rp8.2 trillion, becoming one of PT PP mega project in 2012. Besides, the Company also handled seven airports construction throughout 2012.The Company is committed to perform several corporate action, indicated by corporate action initiation both financially or operationally, namely bonds process as performed by the Company at the end of 2012."
			<br>
			<i style="font-weight: bold;">2013</i>
			<br>
			"To support business development in 2013, the Company has performed several corporate action both financially or operationally, namely: Sustainable Bonds Initial Public Offering, PT PP Dirganeka Acquisition into PP Precast, Property Division Spin off, Branch 8 Offices opening in Celebes, Corporate vision, mission and culture transformation as well as PT Prima Jasa Aldo Dua acquisition plan (under progress)."
		</p>
	</span>
</div>