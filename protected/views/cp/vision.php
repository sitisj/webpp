<?php
/* @var $this CpController */

/*$this->breadcrumbs=array(
	'Company Profile',
);*/
$this->pageTitle=Yii::app()->name . ' | Company Profile';
?>
<?php $this->renderPartial('//cp/submenu'); ?>

<div class="container">
	<h1 style="text-align: center;">Vision</h1>
	<span class="contentcalibri5">
		<p style="text-align: center; font-size: 16px;">
			To be a leader in construction and investment company in Indonesia which Internationally competitive
		</p>
	</span>
	<h1 style="text-align: center;">Mission</h1>
	<span class="contentcalibri5">
		<p style="text-align: center; font-size: 16px;">
			- Providing high added value in construction services to maximize customer satisfaction<br>
			- Increasing the sustainable capability, capacity and welfare of the employee <br>
			- Providing high added value to stakeholders<br>
			- Creating strategic synergy with Vendors, Business Partners and Clients<br>
			- Providing positive contribution to the Environment and Society through the implementation of Green Corporation
		</p>
	</span>
</div>