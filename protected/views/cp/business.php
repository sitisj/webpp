<?php
/* @var $this CpController */

/*$this->breadcrumbs=array(
	'Company Profile',
);*/
$this->pageTitle=Yii::app()->name . ' | Company Profile';
?>
<?php $this->renderPartial('//cp/submenu'); ?>

<div class="container">
	<h1>Business Activities of Company</h1>
	<span class="contentcalibri5">
		<p style="text-align: justify;">
			<br>
			"In business operations through the skills of its workforce and the ability to multi-discipline, PT PP (Persero) Tbk provides various services and providing solutions to clients in every stage of the project activity which owned by Client. With current Business Activity are :"
		</p>
	</span>

	<h2>1. Construction Services</h2>
	<span class="contentcalibri5">
		<p style="text-align: justify;">
			<br>
			"Construction Services PT PP (Persero) Tbk is dealing with Public Construction Services as the core business which includes : High-Rise Buildings, Roads and Bridges, Dams and Irrigations, Hydro Electric and Coal Fire Power Plants, etc. Those Constructions are not merely great value to the Society, but also monumental and landmarks to the Nation. Some project landmarks PT PP (Persero) Tbk is as follows : a.	High-Rise Buildings : Hotel Indonesia - Jakarta, Bali Beach Hotel, Samudera Beach Hotel - Pelabuhan Ratu, Ambarukmo Palace Hotel - Yogyakarta, Bapindo Tower Building - Jakarta, Sapta Pesona Building - Jakarta, BTN Building - Jakarta, Indosat Building - Jakarta, Menara Kuningan Building - Jakarta, Mahkamah Konstitusi RI Building - Jakarta, Departement Agama Building - Jakarta, etc. b. Power Plant HEPP	: Musi Hydroelectric Power Plant - Bengkulu, Tangga Hydro- electric Power Plant Asahan - North Sumatera, Tunnel of Saguling Hydroelectric Power Plant - West Java, Tunnel of Cirata Hydroelectric Power Plant - West Java, Tulis Hydro-electric Power Plant - Central Java, Wonorejo Multipurpose Dam - East Java. SEPP	: Muara Tawar Steam Power Plant - West Java, Suralaya Marine Work Steam Power Plant - West Java, Paiton Steam Power Plant - Central Java. CCPP	: Tambak Lorok Combined Cycle Power Plant - Central Java, Muara Karang Combined Cycle Power Plant - Jakarta, Gresik Combined Cycle Power Plant - East Java, Belawan Combined Cycle Power Plant - North Sumatera. c. Infrastructure Bridges	: Barelang Cable Stayed Bridge - Batam Tonton, Siak Cable Stayed Bridge - Riau, Perawang Bridge - Riau, Kapuas Bridge Pontianak - West Kalimantan. Roads	: Interchange Padalarang Baypass Tollroad - West Java, Sedi - yatmo Tollroad - Jakarta, Senen Underpass - Jakarta, Jakarta Outer Ring Road (JORR), Semarang Northern Ring Road - Jawa Tengah. Railways	: Kabat - Meneng, Bojonegoro - Cepu. Harbours : Koja Wharf - Jakarta,Tanjung Emas Container Port Semarang - Central Java, Cilacap Fishing Port - Central Java, Bajoe Kolaka Crossing Wharf - South Sulawesi, Tanjung Perak Container Wharf Surabaya - East Java, Pertamina Oil Wharf Pelabuhan Panjang - Bandar Lampung, Merak Bakauheni Ferry Terminals - Lampung, Teluk Bayur Harbour - West Sumatera, Sadeng Fishing Port - Central Java. Airports	: Ujung Pandang Airport Makassar - South Sulawesi, Ngurah Rai Bali International Airport Denpasar - Bali, Garuda Maintenance Facilities (GMF) Jakarta International Airport Cengkareng - Jakarta"
		</p>
	</span>

	<h2>2. Property and Realty</h2>
	<span class="contentcalibri5">
		<p style="text-align: justify;">
			<br>
			"PT PP (Persero) Tbk undertakes a business development in the area of Property and Realty by developing Idle Assets of the Company and engages in strategic partnership to create a Property development business such as : Apartments, Hotels, Offices, Malls, Trade Centers and Housings for sale and rental. a. Property. To increase the Added Value of the Company, the Company diversified its business into the Property sector that can be expected to provide a contribution to the Company’s Profit. Property is a business expand in long term character and to have, not for sale but for rent. Property has owned and operated by PT PP (Persero) Tbk include : -	PP Plaza, Jakarta. -	Park Hotel, Jakarta. -	Mall at Kapas Krampung Plaza, Surabaya. b. Realty Realty is a business undertaking of a Developer that is short-term in nature and for sale under a strata-title scheme, and not for ownership. PT PP (Persero) Tbk has sold several of these Developers Projects including among other projects : - Juanda Business Center, Surabaya. -	Patria Park Apartment, Jakarta. -	Paladian Park Apartment, Jakarta. -	Bukit Permata Puri, Semarang. -	Graha Bukopin Building, Surabaya. -	Trade Center at Kapas Krampung Plaza, Surabaya."
		</p>
	</span>

	<h2>3. Investment in Infrastructure Sector</h2>
	<span class="contentcalibri5">
		<p style="text-align: justify;">
			<br>
			"Investment in the Infrastructure Sector has been done through the equity placement of 12.5 % in PT Citra Waspphutowa. This Investment is the construction of the 22.8 km Depok - Antasari Toll Road Project. Currently, the project is in land acquisition process."
		</p>
	</span>

	<h2>4. EPC (Engineering, Procurement & Construction)</h2>
	<span class="contentcalibri5">
		<p style="text-align: justify;">
			<br>
			""
		</p>
	</span>
</div>