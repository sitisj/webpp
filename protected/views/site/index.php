<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
       	<li data-target="#myCarousel" data-slide-to="0"></li>
       	<li data-target="#myCarousel" data-slide-to="1"></li>
       	<li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
    	<div class="item active">
        	<img src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirWebImages'];?>/images/rektoratUI.jpg" alt="First slide">
          	<div class="container">
            	<div class="carousel-caption">
            		<h1>Example headline.</h1>
            		<p>Insert caption</p>
            	</div>
          	</div>
        </div>
        <div class="item">
          	<img src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirWebImages'];?>/images/ccaritb.jpg" alt="Second slide">
          	<div class="container">
            	<div class="carousel-caption">
              		<h1>Another example headline.</h1>
              		<p>Lorem ipsum</p>
            	</div>
          	</div>
        </div>
        <div class="item">
          	<img src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirWebImages'];?>/images/syiahkuala.jpg" alt="Third slide">
          	<div class="container">
            	<div class="carousel-caption">
            		<h1>One more for good measure.</h1>
            		<p>Lorem ipsum</p>
            	</div>
          	</div>
        </div>
    </div>

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
<!-- /.carousel -->

<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing">
    <h1 id="title heading" style="position: center; text-align: center;">Good News!</h1>

    <!-- Three columns of text below the carousel -->
	    <div class="row">
    	    <div class="col-lg-4">
        		<img class="img-circle" src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirWebImages'];?>/images/awards1.jpg" alt="Generic placeholder image" style="width: 140px; height: 140px;">
        		<h2>Heading</h2>
        		<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
        		<p><a class="btn btn-default" href="#" role="button">View details »</a></p>
        	</div><!-- /.col-lg-4 -->
	        <div class="col-lg-4">
	          	<img class="img-circle" src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirWebImages'];?>/images/news2.jpg" alt="Generic placeholder image" style="width: 140px; height: 140px;">
	          	<h2>Heading</h2>
	          	<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
	          	<p><a class="btn btn-default" href="#" role="button">View details »</a></p>
	        </div><!-- /.col-lg-4 -->
	        <div class="col-lg-4">
	          	<img class="img-circle" src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirWebImages'];?>/images/news.jpg" alt="Generic placeholder image" style="width: 140px; height: 140px;">
	          	<h2>Heading</h2>
	          	<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
	          	<p><a class="btn btn-default" href="#" role="button">View details »</a></p>
	        </div><!-- /.col-lg-4 -->
      	</div><!-- /.row -->


      	<!-- START THE FEATURETTES -->

      	<hr class="featurette-divider">

    	<div class="row featurette">
        	<div class="col-md-7">
          		<h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
          		<p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        	</div>
        	<div class="col-md-5">
          		<img class="featurette-image img-responsive" data-src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirJS'];?>/js/holder.js/500x500/auto" alt="500x500" src="">
        	</div>
      	</div>

      	<hr class="featurette-divider">

      	<div class="row featurette">
        	<div class="col-md-5">
          		<img class="featurette-image img-responsive" data-src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirJS'];?>/js/holder.js/500x500/auto" alt="500x500" src="">
        	</div>
        	<div class="col-md-7">
          		<h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
          		<p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        	</div>
      	</div>

      	<hr class="featurette-divider">

      	<div class="row featurette">
        	<div class="col-md-7">
          		<h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
          		<p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        	</div>
        	<div class="col-md-5">
          		<img class="featurette-image img-responsive" data-src="<?php echo Yii::app()->request->baseUrl.Yii::app()->params['dirJS'];?>/js/holder.js/500x500/auto" alt="500x500" src="">
        	</div>
      	</div>

      	<hr class="featurette-divider">

      	<!-- /END THE FEATURETTES -->
    </div>
</div>  

